<?php
/**
 * Plugin Name: ACF Multisite Fields
 * Plugin URI: https://gitlab.com/dkzr-wordpress/plugins/dkzr-acf-multisite-relationship
 * Update URI: https://api.dkzr.nl/wp/update-check/
 * Composer Package Name: dkzr/acf-multisite-relationship
 * Description: Multisite fields for ACF: MS Relationship, MS Post Object and MS User
 * Version: 3.0.0
 * Network: true
 * Author: Joost de Keijzer <j@dkzr.nl>
 * Text Domain: dkzr-acf-ms-relationship
 * Domain Path: languages
 * License: GPL2
 */

/**
 * POC
 * @see ACF-pro/includes/fields/class-acf-field-relationship.php
 * @see ACF-pro/assets/build/js/acf-input.js ("./src/advanced-custom-fields-pro/assets/src/js/_acf-field-relationship.js")
 */

add_action( 'acf/include_field_types', function() {
  if ( is_multisite() ) {
    include_once __DIR__ . '/inc/trait-dkzr-acf-ms-field.php';
    include_once __DIR__ . '/inc/class-dkzr-acf-field-ms-relationship.php';
    include_once __DIR__ . '/inc/class-dkzr-acf-field-ms-post_object.php';
    include_once __DIR__ . '/inc/class-dkzr-acf-field-ms-user.php';
  }
} );
