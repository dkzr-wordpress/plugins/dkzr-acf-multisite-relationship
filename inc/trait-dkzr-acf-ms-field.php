<?php

trait dkzr_acf_ms_field {
  protected $site_fields = []; // ??? unused ¿¿¿
  protected $switched = false;

  protected function maybe_switch_to_blog( $field_key ) {
    $field = acf_get_field( $field_key );

    if ( ! $this->switched && isset( $field['site_id'] ) ) {
      if ( ! isset( $this->site_fields[ $field['site_id'] ] ) ) {
        $this->site_fields[ $field['site_id'] ] = [];
      }

      remove_action( 'switch_blog', 'acf_switch_stores', 10, 2 );
      switch_to_blog( $field['site_id'] );
      add_action( 'switch_blog', 'acf_switch_stores', 10, 2 );

      $this->switched = true;
    }
  }

  protected function maybe_restore_current_blog( $field_key ) {
    $field = acf_get_field( $field_key );

    if ( $this->switched && isset( $field['site_id'] ) ) {
      if ( isset( $this->site_fields[ $field['site_id'] ][ $field_key ] ) ) {
        remove_filter( sprintf( 'acf/load_field/key=%s', $field_key ), $this->site_fields[ $field['site_id'] ][ $field_key ] );
      }
      remove_action( 'switch_blog', 'acf_switch_stores', 10, 2 );
      restore_current_blog();
      add_action( 'switch_blog', 'acf_switch_stores', 10, 2 );

      $this->switched = false;
    }
  }
}
