<?php

if ( ! class_exists( 'dkzr_acf_field_ms_post_object' ) && class_exists( 'acf_field_post_object' ) ) :

  class dkzr_acf_field_ms_post_object extends acf_field_post_object {
    use dkzr_acf_ms_field;

    function initialize() {
      // vars
      $this->name     = 'ms_post_object';
      $this->label    = __( 'Multisite Post Object', 'acf' );
      $this->category = 'relational';
      $this->defaults = array(
        'site_id'       => 0,
        'post_type'     => array(),
        'taxonomy'      => array(),
        'allow_null'    => 0,
        'multiple'      => 0,
        'return_format' => 'object',
        'ui'            => 1,
      );

      // these actions overrule the original `wp_ajax[_nopriv]_acf/fields/post_object/query` actions!
      add_action( 'wp_ajax_acf/fields/post_object/query', array( $this, 'ajax_query' ), 5 );
      add_action( 'wp_ajax_nopriv_acf/fields/post_object/query', array( $this, 'ajax_query' ), 5 );

      // make the original JS trigger on this field type
      add_filter( 'acf/field_wrapper_attributes', [ $this, 'field_wrapper_attributes' ], 10, 2 );
    }

    function field_wrapper_attributes( $wrapper, $field ) {
      if ( 'ms_post_object' == $field['type'] ) {
        $wrapper['data-type'] = 'post_object';
      }

      return $wrapper;
    }

    function get_ajax_query( $options = array() ) {
      $this->maybe_switch_to_blog( $options['field_key'] );

      $response = parent::get_ajax_query( $options );

      $this->maybe_restore_current_blog( $options['field_key'] );

      return $response;
    }

    function get_post_title( $post, $field, $post_id = 0, $is_search = 0, $unescape = false ) {
      $this->maybe_switch_to_blog( $field['key'] );

      $title = parent::get_post_title( $post, $field, $post_id, $is_search, $unescape );

      $this->maybe_restore_current_blog( $field['key'] );

      return $title;
    }

    function render_field( $field ) {
      $this->maybe_switch_to_blog( $field['key'] );

      parent::render_field( $field );

      $this->maybe_restore_current_blog( $field['key'] );
    }

    function render_field_settings( $field ) {
      $sites = [];
      foreach( get_sites( [ 'public' => 1, 'archived' => 0, 'deleted' => 0 ] ) as $site ) {
        $sites[ $site->blog_id ] = sprintf( '%s (ID %d &mdash; %s)', $site->blogname, $site->id, $site->siteurl );
      }

      $this->maybe_switch_to_blog( $field['key'] );

      acf_render_field_setting(
        $field,
        array(
          'label'        => __( 'Site ID', 'acf' ),
          'instructions' => '',
          'type'         => 'select',
          'name'         => 'site_id',
          'choices'      => $sites,
          'multiple'     => 0,
          'ui'           => 1,
          'allow_null'   => 1,
          'placeholder'  => __( 'Select Site', 'acf' ),
        )
      );

      parent::render_field_settings( $field );

      $this->maybe_restore_current_blog( $field['key'] );
    }

    function format_value( $value, $post_id, $field ) {
      $this->maybe_switch_to_blog( $field['key'] );

      $value = parent::format_value( $value, $post_id, $field );

      $this->maybe_restore_current_blog( $field['key'] );

      return $value;
    }

    function get_posts( $value, $field ) {
      $this->maybe_switch_to_blog( $field['key'] );

      $posts = parent::get_posts( $value, $field );

      $this->maybe_restore_current_blog( $field['key'] );

      return $posts;
    }

    // TODO: untested
    public function validate_rest_value( $valid, $value, $field ) {
      $this->maybe_switch_to_blog( $field['key'] );

      $result = parent::validate_rest_value( $valid, $value, $field );

      $this->maybe_restore_current_blog( $field['key'] );

      return $result;
    }

    // TODO: untested
    public function get_rest_links( $value, $post_id, array $field ) {
      $this->maybe_switch_to_blog( $field['key'] );

      $links = parent::get_rest_links( $value, $post_id, $field );

      $this->maybe_restore_current_blog( $field['key'] );

      return $links;
    }
  }

  acf_register_field_type( 'dkzr_acf_field_ms_post_object' );

endif; // class_exists check
