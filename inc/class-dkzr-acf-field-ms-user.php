<?php

if ( ! class_exists( 'dkzr_acf_field_ms_user' ) && class_exists( 'ACF_Field_User' ) ) :

  class dkzr_acf_field_ms_user extends ACF_Field_User {
    function initialize() {
      // vars
      $this->name     = 'ms_user';
      $this->label    = __( 'Multisite User', 'acf' );
      $this->category = 'relational';
      $this->defaults = array(
        'role'          => '',
        'multiple'      => 0,
        'allow_null'    => 0,
        'return_format' => 'array',
      );

      // these actions overrule the original `wp_ajax[_nopriv]_acf/fields/post_object/query` actions!
      add_action( 'wp_ajax_acf/fields/user/query', array( $this, 'ajax_query' ), 5 );
      add_action( 'wp_ajax_nopriv_acf/fields/user/query', array( $this, 'ajax_query' ), 5 );

      // make the original JS trigger on this field type
      add_filter( 'acf/field_wrapper_attributes', [ $this, 'field_wrapper_attributes' ], 10, 2 );
    }

    function field_wrapper_attributes( $wrapper, $field ) {
      if ( 'ms_user' == $field['type'] ) {
        $wrapper['data-type'] = 'user';
      }

      return $wrapper;
    }

    function ajax_query() {
      add_filter( 'acf/ajax/query_users/args', array( $this, 'ajax_query_args' ), 9, 3 );

      // force flat list
      acf_get_instance( 'ACF_Ajax_Query_Users' )->is_search = true;
      parent::ajax_query();
    }

    function ajax_query_args( $args, $request, $query ) {
      $args['blog_id'] = 0;

      return $args;
    }

    function render_field( $field ) {
      // Change Field into a select.
      $field['type']           = 'select';
      $field['_original_type'] = $this->name;
      $field['ui']             = 1;
      $field['ajax']           = 1;
      $field['choices']        = array();
      $field['query_nonce']    = wp_create_nonce( 'acf/fields/user/query' . $field['key'] );

      // Populate choices.
      if ( $field['value'] ) {

        // Clean value into an array of IDs.
        $user_ids = array_map( 'intval', acf_array( $field['value'] ) );

        // Find users in database (ensures all results are real).
        $users = acf_get_users(
          array(
            'include' => $user_ids,
            'blog_id' => 0,
          )
        );

        // Append.
        if ( $users ) {
          foreach ( $users as $user ) {
            $field['choices'][ $user->ID ] = $this->get_result( $user, $field );
          }
        }
      }

      // Render.
      acf_render_field( $field );
    }

    function format_value( $value, $post_id, $field ) {

      // Bail early if no value.
      if ( ! $value ) {
        return false;
      }

      // Clean value into an array of IDs.
      $user_ids = array_map( 'intval', acf_array( $value ) );

      // Find users in database (ensures all results are real).
      $users = acf_get_users(
        array(
          'include' => $user_ids,
          'blog_id' => 0,
        )
      );

      // Bail early if no users found.
      if ( ! $users ) {
        return false;
      }

      // Format values using field settings.
      $value = array();
      foreach ( $users as $user ) {

        // Return object.
        if ( $field['return_format'] == 'object' ) {
          $item = $user;

          // Return array.
        } elseif ( $field['return_format'] == 'array' ) {
          $item = array(
            'ID'               => $user->ID,
            'user_firstname'   => $user->user_firstname,
            'user_lastname'    => $user->user_lastname,
            'nickname'         => $user->nickname,
            'user_nicename'    => $user->user_nicename,
            'display_name'     => $user->display_name,
            'user_email'       => $user->user_email,
            'user_url'         => $user->user_url,
            'user_registered'  => $user->user_registered,
            'user_description' => $user->user_description,
            'user_avatar'      => get_avatar( $user->ID ),
          );

          // Return ID.
        } else {
          $item = $user->ID;
        }

        // Append item
        $value[] = $item;
      }

      // Convert to single.
      if ( ! $field['multiple'] ) {
        $value = array_shift( $value );
      }

      // Return.
      return $value;
    }
  }

  acf_register_field_type( 'dkzr_acf_field_ms_user' );

endif; // class_exists check
